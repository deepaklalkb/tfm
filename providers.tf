provider "azurerm" {
  features {}
}
terraform {
  backend "azurerm" {
    resource_group_name   = "yourresourcegroup"
    storage_account_name  = "storageaccounttfm1"
    container_name        = "terraform-state"
    key                   = "test.terraform.tfstate"
  }
}
